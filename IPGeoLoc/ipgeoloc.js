//-------------------------------------------------------------------------
// Message type constants.
//
//   Notes: These values must be synchronized with ipgeoloc.h
//-------------------------------------------------------------------------
const GET_SERVER_VERSION   = '0';
const GET_PCAP_VERSION     = '1';
const GET_BPF_STRING       = '2';
const SET_BPF_STRING       = '3';
const GET_DNS_SNOOPING     = '4';
const ENABLE_DNS_SNOOPING  = '5';
const DISABLE_DNS_SNOOPING = '6';
const CAPTURED_PACKET      = '7';
const SNOOPED_DNS_VALUE    = '8';
const SET_GOOGLE_API_KEY   = '9';
const UNKNOWN_ERROR        = 'A';

var CNXs = {};
const defaultTime = 30;
var ws;
var google_api_key = "Uninitialized";

function getGoogleMapsAPIURLKey() {
	var keyedURL;
	
	waitPeriod = 5;
	while ( google_api_key == "Uninitialized") {
	}
	if ( google_api_key == "Uninitialized" ) {
		alert("Google API Key never got set ... this isn't gonna work!");
	}
	keyedURL = "https://maps.googleapis.com/maps/api/js?key=" + google_api_key + "&callback=initMap"
	return google_api_key;
}

function decimalToHex(d, padding) {
    var hex = Number(d).toString(16);
    padding = typeof (padding) === "undefined" || padding === null ? padding = 2 : padding;

    while (hex.length < padding) {
        hex = "0" + hex;
    }

    return hex;
}

function fadeCNXs() {
   for( connection in CNXs ) {
   
      var opacityNew = (CNXs[connection].conn.strokeOpacity - CNXs[connection].tsteps);
      CNXs[connection].conn.setOptions({strokeOpacity: opacityNew});

   }
}

function deleteCNX(id) {
   if( id in CNXs ) {
      CNXs[id].conn.setMap(null);
      google.maps.event.clearInstanceListeners(CNXs[id].conn);
      CNXs[id].conn = null;
      delete CNXs[id];
   }
}

function deleteOldCNXs() {
   for( connection in CNXs ) {
      CNXs[connection].timer--;
//      alert("Connection Timer: " + CNXs[connection].timer);
      if( CNXs[connection].timer == 0 ) {
         deleteCNX(connection);
      }
   }
	
}

function tickTock() {
   deleteOldCNXs();
   fadeCNXs();
}

//-------------------------------------------------------------------------
// processMessage
//   Parameters: The string received from the IPGeoLoc Server
//   Returns:    Nothing
//   Purpose:    To determine the message type and invoke the appropriate method.
//   Method:     Split the string into fields using '|' as a field delimiter.
//               The first field of the message defines the message type.
//               Extract the first field and assign to 'msg_type'
//               Shift the remaining fields (gets rid of first field)
//               Use switch to invoke appropriate method.
//   Notes:      Some message types don't make sense to the UI - just ignore them.
//-------------------------------------------------------------------------
function processMessage(someString) {
   var x = someString.split("|");
   var msg_type = x[0];
   x.shift();

   switch (msg_type) {
   case GET_SERVER_VERSION:
	    console.log("SERVER VERSION: " + x);
	    alert("SERVER VERSION: " + x);
	    break;
   case GET_PCAP_VERSION:
	    console.log("PCAP VERSION: " + x);
	    alert("PCAP VERSION: " + x);
	    break;
   case GET_BPF_STRING:
	    console.log("Packet Filter: " + x);
	    alert("Packet Filter: " + x);
	    break;
   case SET_BPF_STRING:
	    console.log("Packet Filter: " + x);
	    alert("Packet Filter: " + x);
	    break;
   case GET_DNS_SNOOPING:
	    console.log(x);
	    alert(x);
	    break;
   case ENABLE_DNS_SNOOPING:
	    console.log("ENABLE_DNS_SNOOPING");
	    break;
   case DISABLE_DNS_SNOOPING:
	    console.log("DISABLE_DNS_SNOOPING");
	    break;
   case CAPTURED_PACKET:
	    console.log("CAPTURED_PACKET");
        createOrUpdateCNX(someString.substring(2));
        break;
   case SNOOPED_DNS_VALUE:
	    console.log("SNOOPED_DNS_VALUE");
        break;
   case SET_GOOGLE_API_KEY:
	    console.log("SET_GOOGLE_API_KEY:" + x);
	    google_api_key = x[0];
		var JSLink = "https://maps.googleapis.com/maps/api/js?key=" + google_api_key + "&callback=initMap ";
		var JSElement = document.createElement('script');
		JSElement.src = JSLink;
		<!-- JSElement.onload = OnceLoaded; -->
		document.getElementsByTagName('head')[0].appendChild(JSElement);
	    break;
   case UNKNOWN_ERROR:
	    console.log("UNKNOWN_ERROR");
        break;
   default:
        console.log("Unknown Message Type: " + msg_type + " in message " + someString);
        break;
   }
}


//-------------------------------------------------------------------------
// Function:createOrUpdateCNX
//   Parameters: The string received from the IPGeoLoc Server
//   Returns:    connectionInfo object
//   Purpose:    To create an object to track a connection.
//   Method:     Parse the parameter into time, 
//                                        protocol, 
//                                        Source IP, 
//                                        Source Port, 
//                                        Destination IP, 
//                                        Destination Port, 
//                                        Source Lat/Lng, 
//                                        Destination Lat/Lng
//               Generate a hashKey to save/recall the connection attrs.
//               Check the CNXs hash to see if this member already exists
//               If member doesn't already exist:
//                  Create endPoints array.
//                  Create new Polyline using endPoints.
//                  Create hash member containing connection attrs.
//               Increment hash member packet count
//               Update hash member time & timer attributes
//               Return hash member
//   Notes:      A member is the combination of protocol, IP & Ports
//               - It does not include the direction of the packet.
//                 - This means we have to use the same CNXs member
//                   for two different strings from IPGeoLoc.
//
// *** 2018.04.10: Changing to a single polyline per src/dst IP pair.
//         ToDo:   Move connection tracking (port pairs) into a data
//                 struct within the IP pair.
//-------------------------------------------------------------------------
function createOrUpdateCNX(someString) {
   var x = someString.split("|");
   var ipProtocol = x[1];
   var ipSrcAddr  = x[2];
   var SrcPortNum = x[3];
   var ipDstAddr  = x[4];
   var DstPortNum = x[5];
   var lat1 = x[6].split(",")[0];
   var lng1 = x[6].split(",")[1];
   var lat2 = x[7].split(",")[0];
   var lng2 = x[7].split(",")[1];
   var sclr = 0xFFFFFF;
   var hashKey = "";
// console.log("createOrUpdateCNX: " + lat1 + " " + lng1 + " " + lat2 + " " + lng2);


   // Normalize the array entry by always storing the lower IP/Port first.
   // This way we don't have separate objects for traffic in each direction.
   if ( ipSrcAddr < ipDstAddr ) {
      lowIP    = ipSrcAddr;
      lowPort  = SrcPortNum;
      highIP   = ipDstAddr;
      highPort = DstPortNum; }
   else {
      lowIP    = ipDstAddr;
      lowPort  = DstPortNum;
      highIP   = ipSrcAddr;
      highPort = SrcPortNum; }

   // Construct the key.
   hashKey = ipProtocol + lowIP + lowPort + highIP + highPort;

   // Check to see if connection is already tracked.
   if ( ! CNXs.hasOwnProperty(hashKey) ) {
      switch (ipProtocol) {
      case  "6": sclr = "#0000FF";
                 break;
      case "17": sclr = "#FF0000";
                 break;
      default:   sclr = "#FFFFFF";
                 break;
      }
      var endPoints = [
         new google.maps.LatLng(lat1, lng1),
         new google.maps.LatLng(lat2, lng2)
         ];

      var cnx = new google.maps.Polyline({
         path: endPoints,
         geodesic: true,
         strokeColor: sclr,
         strokeOpacity: 1.0,
         strokeWeight: 2
         });

      var connectionInfo = {
         conn:      cnx,
         tsteps:    1.0 / defaultTime,
         srcPcktCtr: 0,
         dstPcktCtr: 0,
         proto:     ipProtocol,
         srcip:     ipSrcAddr,
         srcport:   SrcPortNum,
         dstip:     ipDstAddr,
         dstport:   DstPortNum,
         srcloc:    x[6],
         dstloc:    x[7],
         srcdns:    "",
         dstdns:    ""
         };
      CNXs[hashKey] = connectionInfo;
      
      cnx.addListener('click', function() {
         // We want to pan the map to show the connection.
         // That means we need to find the North & South extents
         // and the East & West extents of the connection
         
         if(lat1 < lat2) {
            latS = lat1;
            latN = lat2; }
         else {
            latS = lat2;
            latN = lat1; }

         if ( lng1 > 0 && lng2 > 0) {
            // Eastern Hemisphere
            lngW = lng1 > lng2 ? lng2 : lng1;
            lngE = lng1 > lng2 ? lng1 : lng2; }
         else if ( lng1 < 0 && lng2 < 0 ) {
            // Western Hemisphere
            lngW = lng1 > lng2 ? lng1 : lng2;
            lngE = lng1 > lng2 ? lng2 : lng1; }
         else {
            // Crosses either date line or prime meridian.
            // We need to find the shortest arc between the two points.
            // lng1 > 0 means lng1 is in Eastern Hem.
            if ( lng1 > 0 ) {
               // Measure from East to West
               if ( lng1 - lng2 <= 180 ) {
                  lngW = lng2;
                  lngE = lng1; }
               else {
                  lngW = lng1;
                  lngE = lng2; }
               }
            else {
               // lng1 is in Western Hem.
               if ( lng2 - lng1 <= 180 ) {
                  // Measure from West to East
                  lngW = lng1;
                  lngE = lng2; }
               else {
                  lngW = lng2;
                  lngE = lng1; }
               }
            }
         var cnxBounds = new google.maps.LatLngBounds(new google.maps.LatLng(latS, lngW), new google.maps.LatLng(latN, lngE));
         map.panToBounds(cnxBounds);

         var A_SideStats = '<div class="EndPointStats">'+
                           '<p>DNS:' + CNXs[hashKey].srcdns + '<br>' +
                           CNXs[hashKey].srcloc + '<br>' +
                           'IP[:Port]' + CNXs[hashKey].srcip + ':' + CNXs[hashKey].srcport + '<br>' +
                           'Packets Sent: ' + CNXs[hashKey].srcPcktCtr  + '</p>' +
                           '</div>';

         var Z_SideStats = '<div class="EndPointStats">'+
                           '<p>DNS:' + CNXs[hashKey].dstdns + '<br>' +
                           CNXs[hashKey].dstloc + '<br>' + 
                           'IP[:Port]' + CNXs[hashKey].dstip + ':' + CNXs[hashKey].dstport + '<br>' +
                           'Packets Sent: ' + CNXs[hashKey].dstPcktCtr + '</p>' +
                           '</div>';

         var infowindowA = new google.maps.InfoWindow({ content: A_SideStats, position: endPoints[0] });
         var infowindowZ = new google.maps.InfoWindow({ content: Z_SideStats, position: endPoints[1] });
         infowindowA.open(map);
         infowindowZ.open(map);
         });

      cnx.setMap(map);
   }
   CNXs[hashKey].timer = defaultTime;
   CNXs[hashKey].time  = x[0];
   if (ipSrcAddr == CNXs[hashKey].srcip ) {
      CNXs[hashKey].srcPcktCtr++; }
   else {
      CNXs[hashKey].dstPcktCtr++; }

   return CNXs[hashKey];
}

function showContextMenu(currentLatLng) {
    var projection;
    var contextmenuDir;
    console.log("In showContextMenu");
    projection = map.getProjection() ;
    $('.contextmenu').remove();
     contextmenuDir = document.createElement("div");
      contextmenuDir.className  = 'contextmenu';
      contextmenuDir.innerHTML = '<a id="menu1"><div class="context">menu item 1<\/div><\/a>'
                              + '<a id="menu2"><div class="context">menu item 2<\/div><\/a>';

    $(map.getDiv()).append(contextmenuDir);
 
    setMenuXY(currentLatLng);

    contextmenuDir.style.visibility = "visible";
}


window.setInterval(function(){ tickTock(); }, 1000);
  
if ("WebSocket" in window) {
   // Let us open a web socket
   ws = new WebSocket("ws://localhost:8081/");

   ws.onopen = function() {
      // Web Socket is connected, send data using send()
      console.log("WebSocket Open");
      ws.send(SET_BPF_STRING + "|filterexp = \"not ether host FF:FF:FF:FF:FF:FF and not host 70.75.248.74\""); };

   ws.onmessage = function (evt) { 
      var received_msg = evt.data;
      console.log("Received msg: " + received_msg);
      var this_packet = processMessage(received_msg); };
		
   ws.onclose = function() {
      // websocket is closed.
      // alert("Connection is closed...");
      console.log("Connection is closed..."); };
   }
else {
   // The browser doesn't support WebSocket
   alert("WebSocket NOT supported by your Browser!"); }


