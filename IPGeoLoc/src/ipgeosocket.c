/*
 * ipgeosocket.c
 *
 *  Created on: 2015-07-22
 *      Author: jardine
 */

#include <pthread.h>
#include "common.h"
#include "ipgeosocket.h"

static bool thread_exists = false;

bool ipgeosocket_listen(void) {
   bool rtn_value = false;

   if (! thread_exists ) {
      if (create_thread()) {
         if ( start_thread() ) {
            thread_exists = true;
            rtn_value = true; }
         else {
            stop_thread();
            }
         }
      }
   return rtn_value;
}

bool ipgeosocket_close(void) {
   bool rtn_value = false;
   if( stop_thread() ) {
      rtn_value = true;
      thread_exists = false;
   }
   return rtn_value;
}




static void * socket_service(void * params) {
   int rtn_value = 0;

   return &rtn_value;
}

static bool create_thread(void) {
   bool rtn_value = false;
   int  pthread_error;

   pthread_t thread;
   if ( (pthread_error = pthread_create(&thread, NULL, &socket_service, NULL)) ) {
      rtn_value = false; }
   else {
      rtn_value = true; }
   return rtn_value;
}


static start_thread() {

}

static bool stop_thread() {
   bool rtn_value = false;

   return rtn_value;
}


