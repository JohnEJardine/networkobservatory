/*
 * tries.c
 *
 *  Created on: 2015-04-24
 *      Author: jardine
 */
#include <stddef.h>
#include <arpa/inet.h>
#include "common.h"
#include "tries.h"

#define DEFAULT_LATITUDE  -71.966506
#define DEFAULT_LONGITUDE  23.378906





#include <stdio.h>
#define JEJ_MAX_DBG_STRLEN   132
char debugMsgBuffer[JEJ_MAX_DBG_STRLEN];
static struct in_addr targetIP;





static void tries_initialize(struct trie_latlngEntry *);
static void tries_add_i(u_int32_t, u_int32_t, u_int32_t, u_int32_t, float, float);
struct trie_latlngEntry *trie_base;
u_int32_t next_free_t_idx;

static int initialized = 0;

//static u_int32_t masks[] = { 0x00000000, 0x80000000, 0xC0000000, 0xE0000000, 0xF0000000, 0xF8000000, 0xFC000000, 0xFE000000,
//         0xFF000000, 0xFF800000, 0xFFC00000, 0xFFE00000, 0xFFF00000, 0xFFF80000, 0xFFFC0000, 0xFFFE0000, 0xFFFF0000, 0xFFFF8000,
//         0xFFFFC000, 0xFFFFE000, 0xFFFFF000, 0xFFFFF800, 0xFFFFFC00, 0xFFFFFE00, 0xFFFFFF00, 0xFFFFFF80, 0xFFFFFFC0, 0xFFFFFFE0,
//         0xFFFFFFF0, 0xFFFFFFF8, 0xFFFFFFFC, 0xFFFFFFFE, 0xFFFFFFFF };


/*-------------------------------------------------------------------------*/
/* We cheat and create an array of 256 entries for the first octet.        */
/*-------------------------------------------------------------------------*/
static void tries_initialize(struct trie_latlngEntry *t_base) {
   initialized = 1;

   inet_pton(AF_INET, "70.75.236.94", &targetIP); //delme

   trie_base = t_base;
   for (int idx = 0; idx < 256; idx++) {
      t_base[idx].zero = 0;
      t_base[idx].one  = 0;
      t_base[idx].lat  = DEFAULT_LATITUDE;
      t_base[idx].lng  = DEFAULT_LONGITUDE;
   }
   next_free_t_idx = 256;
}

/*-------------------------------------------------------------------------*/
/* This function is invoked with parameters describing where we are and    */
/* and where we need to stop.                                              */
/* Operation:                                                              */
/* - Check if we're at the right level                                     */
/* Add a trie entry                                                        */
/* This may involve walking the trie to find the insertion point           */
/*-------------------------------------------------------------------------*/
static void tries_add_i(u_int32_t ipAddress, u_int32_t thisNetMaskBit, u_int32_t targetNetMaskBit, u_int32_t this_trie, float lat, float lng) {
   u_int32_t next_trie;

   /*----------------------------------------------------------------------*/
   /* Do we need the 'zero' or 'one' trie pointer?                         */
   /* If the trie doesn't exist - allocate and associate a new one.        */
   /* Copy lat/long values from current trie to new trie                   */
   /*----------------------------------------------------------------------*/
   if ( ipAddress & thisNetMaskBit) {
      if ( trie_base[this_trie].one == 0 ) {
         trie_base[this_trie].one  = next_free_t_idx++;
         next_trie                 = trie_base[this_trie].one;
         trie_base[next_trie].zero = 0;
         trie_base[next_trie].one  = 0;
         trie_base[next_trie].lat  = trie_base[this_trie].lat;
         trie_base[next_trie].lng  = trie_base[this_trie].lng;
         }
      else {
         next_trie = trie_base[this_trie].one;
         }
      }
   else {
      if ( trie_base[this_trie].zero == 0 ) {
         trie_base[this_trie].zero  = next_free_t_idx++;
         next_trie                 = trie_base[this_trie].zero;
         trie_base[next_trie].zero = 0;
         trie_base[next_trie].one  = 0;
         trie_base[next_trie].lat  = trie_base[this_trie].lat;
         trie_base[next_trie].lng  = trie_base[this_trie].lng;
         }
      else {
         next_trie = trie_base[this_trie].zero;
         }
      }

   /*----------------------------------------------------------------------*/
   /* Shift the mask bit 1 position before checking if we need to recurse. */
   /*----------------------------------------------------------------------*/
   if( thisNetMaskBit != targetNetMaskBit) {
      thisNetMaskBit >>= 1;
      tries_add_i(ipAddress, thisNetMaskBit, targetNetMaskBit, next_trie, lat, lng); }
   else {
      trie_base[next_trie].lat = lat;
      trie_base[next_trie].lng = lng;
      }
   }

/*-------------------------------------------------------------------------*/
/* This function adds a trie describing the network address passed in      */
/* ipAddress as ntohl() with netmask.                                                 */
/*-------------------------------------------------------------------------*/
int tries_add(void *buffer, u_int32_t ipAddress, int netmask, float lat, float lng) {
   int rtn_value = -1;
   unsigned char first_octet;

   /*----------------------------------------------------------------------*/
   /* Initialize the trie data structure                                   */
   /*----------------------------------------------------------------------*/
   if (!initialized) {
      tries_initialize((struct trie_latlngEntry *) buffer);
   }

   /*----------------------------------------------------------------------*/
   /* The first octet is the base of the trie we will add to.              */
   /* If the netmask is bigger 8, then we will need to extend the trie     */
   /* until it is at least netmask bits deep.                              */
   /*----------------------------------------------------------------------*/
   first_octet = ((ipAddress >> 24) & 0xFF);
   if (netmask > 8) {
      u_int32_t targetNetMaskBit = (0x80000000 >> (netmask - 1));
      tries_add_i(ipAddress, 0x00800000, targetNetMaskBit, first_octet, lat, lng);
   }
   else {
      trie_base[first_octet].lat = lat;
      trie_base[first_octet].lng = lng;
   }
   return (rtn_value);
}

/*-------------------------------------------------------------------------*/
/* Function: tries_find                                                    */
/* Parameters: u_int32_t ipAddress                                         */
/* Returns:    struct trie_latlngEntry *                                   */
/* Notes:      Navigate trie data structure based on ipAddress             */
/*             Return pointer to closest trie.                             */
/*-------------------------------------------------------------------------*/
struct trie_latlngEntry *tries_find(u_int32_t ipAddress) {
   struct trie_latlngEntry *rtn_val;
   u_int32_t trie_idx;
   u_int32_t netmask, nextBit;
   u_int32_t targetIP = ntohl(ipAddress);


   /*----------------------------------------------------------------------*/
   /* Get trie for first octet                                             */
   /* Then, if down > 0, get the trie for the 9th bit                      */
   /*----------------------------------------------------------------------*/
   trie_idx = ((targetIP >> 24) & 0xFF);
   netmask  = 0x00800000;
   bool found = false;
   while (! found) {
      nextBit  = targetIP & netmask;
      netmask  >>= 1;
      if (nextBit) {
         if (trie_base[trie_idx].one > 0) {
            trie_idx = trie_base[trie_idx].one; }
         else {
            rtn_val = &(trie_base[trie_idx]);
            found = true;}
         }
      else {
         if (trie_base[trie_idx].zero > 0) {
            trie_idx = trie_base[trie_idx].zero; }
         else {
            rtn_val = &(trie_base[trie_idx]);
            found = true;}
         }
      }

   return rtn_val;
   }


u_int32_t tries_count() {
   return next_free_t_idx;
}

