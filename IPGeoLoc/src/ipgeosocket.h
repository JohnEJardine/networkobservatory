/*
 * ipgeosocket.h
 *
 *  Created on: 2015-07-22
 *      Author: jardine
 */

#ifndef IPGEOSOCKET_H_
#define IPGEOSOCKET_H_
#ifndef COMMON_H_
#include "common.h"
#endif

bool ipgeosocket_listen(void);
bool ipgeosocket_close(void);

#endif /* IPGEOSOCKET_H_ */
