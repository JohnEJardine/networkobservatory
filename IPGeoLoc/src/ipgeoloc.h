/*
 * ipgeoloc.h
 *
 *  Created on: 2015-04-17
 *      Author: jardine
 */

#ifndef IPGEOLOC_H_
#define IPGEOLOC_H_

#define MAX_GOOGLE_MAPS_API_KEYLENGTH 1024

#define NO_SERVER_VERSION    "0.10"

#define GET_SERVER_VERSION   '0'
#define GET_PCAP_VERSION     '1'
#define GET_BPF_STRING       '2'
#define SET_BPF_STRING       '3'
#define GET_DNS_SNOOPING     '4'
#define ENABLE_DNS_SNOOPING  '5'
#define DISABLE_DNS_SNOOPING '6'
#define CAPTURED_PACKET      '7'
#define SNOOPED_DNS_VALUE    '8'
#define SET_GOOGLE_API_KEY   '9'
#define UNKNOWN_ERROR        'A'

struct trie_latlngEntry {
   float lat;
   float lng;
   u_int32_t zero;
   u_int32_t one;
};

#endif /* IPGEOLOC_H_ */
