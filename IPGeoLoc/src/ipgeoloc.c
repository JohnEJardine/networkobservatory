/*
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/ether.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <pcap.h>
#include <poll.h>
#include "ipgeoloc.h"
#include "tries.h"
//#include "ipgeosocket.h"
#include "common.h"

#define TCPIPHEADERLENGTH    66
#define JEJ_MAX_DBG_STRLEN   132
#define MIN(X, Y)  ((X) < (Y) ? (X) : (Y))

static unsigned int debuglvl   = 0;
static unsigned int warninglvl = 0;
char debugMsgBuffer[JEJ_MAX_DBG_STRLEN];
//extern int errno;
static struct timeval currentPktTS, previousPktTS;
static char googleMapsAPIKey[MAX_GOOGLE_MAPS_API_KEYLENGTH];

void debug_msg(char *msg) {
   if (debuglvl > 0) {
      fprintf(stderr, "%s", msg);
   }
}

void warning_msg(char *msg) {
   if (warninglvl > 0) {
      fprintf(stderr, "%s", msg);
   }
}

void error_msg(char *msg) {
   fprintf(stderr, "%s", msg);
   exit(-1);
}

void process_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) {
   static u_int pkt_cntr = 0;
   struct ether_header *ethhdr = (struct ether_header *) packet;
   struct iphdr *iphdr = (void *) packet + sizeof(struct ether_header);
   struct tcphdr *tcpudphdr = (void *) iphdr + sizeof(struct iphdr);

   struct tm *nowtm;
   char tmbuf[64];
   char srcAddrStr[INET_ADDRSTRLEN], dstAddrStr[INET_ADDRSTRLEN];

   /*----------------------------------------------------------------------*/
   /* Increment the packet counter.                                        */
   /*----------------------------------------------------------------------*/
   pkt_cntr++;

   /*----------------------------------------------------------------------*/
   /* Does user want to capture packets to file?                           */
   /*----------------------------------------------------------------------*/
   if (args) {
      pcap_dump(args, header, packet );
   }

   /*----------------------------------------------------------------------*/
   /* Keep track of the packet time-stamps.  main() needs these to calc    */
   /* a packet delay during a capture playback.                            */
   /*----------------------------------------------------------------------*/
   previousPktTS.tv_sec  = currentPktTS.tv_sec;
   previousPktTS.tv_usec = currentPktTS.tv_usec;
   currentPktTS.tv_sec   = header->ts.tv_sec;
   currentPktTS.tv_usec  = header->ts.tv_usec;

   /*----------------------------------------------------------------------*/
   /* String-ify the date & time.                                          */
   /*----------------------------------------------------------------------*/
   nowtm = localtime(&(header->ts.tv_sec));
   strftime(tmbuf, sizeof tmbuf, "%Y-%m-%d %H:%M:%S", nowtm);
   snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "%u: Time:%s.%06ld Capture Length:%i Packet Length:%i", pkt_cntr, tmbuf, header->ts.tv_usec, header->caplen, header->len);
//   debug_msg(debugMsgBuffer);

   /*----------------------------------------------------------------------*/
   /* Verify we're looking at an IP packet, message & ignore otherwise.    */
   /*----------------------------------------------------------------------*/
   if (ntohs(ethhdr->ether_type) != ETHERTYPE_IP) {
      snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, ". NON IP Ethernet packet: From:%s To:%s Type:0x%04X\n", ether_ntoa((struct ether_addr*) ethhdr->ether_shost),
      ether_ntoa((struct ether_addr*) ethhdr->ether_dhost), ntohs(ethhdr->ether_type));
      debug_msg(debugMsgBuffer);
      return;
   }

   /*----------------------------------------------------------------------*/
   /* Retrieve the Source and Destination IP addresses.                    */
   /*----------------------------------------------------------------------*/
   inet_ntop(AF_INET, &(iphdr->saddr), srcAddrStr, INET_ADDRSTRLEN);
   inet_ntop(AF_INET, &(iphdr->daddr), dstAddrStr, INET_ADDRSTRLEN);

   /*----------------------------------------------------------------------*/
   /* Lookup the geolocation for these IP Addresses.                       */
   /*----------------------------------------------------------------------*/
   struct trie_latlngEntry *src_location_trie, *dst_location_trie;
   src_location_trie = tries_find(iphdr->saddr);
   dst_location_trie = tries_find(iphdr->daddr);

   /*----------------------------------------------------------------------*/
   /* Retrieve the Source and Destination Port numbers (if TCP or UDP).    */
   /*----------------------------------------------------------------------*/
   if (iphdr->protocol == IPPROTO_TCP || iphdr->protocol == IPPROTO_UDP) {
      snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, " %s:0x%04X => %s:0x%04X", srcAddrStr, ntohs(tcpudphdr->source), dstAddrStr, ntohs(tcpudphdr->dest));
      debug_msg(debugMsgBuffer);
      printf("%c|%s.%06ld|%u|%s|%u|%s|%u|%f, %f|%f, %f\n", CAPTURED_PACKET, tmbuf, header->ts.tv_usec, iphdr->protocol, srcAddrStr, ntohs(tcpudphdr->source), dstAddrStr, ntohs(tcpudphdr->dest), src_location_trie->lat, src_location_trie->lng, dst_location_trie->lat, dst_location_trie->lng);
   }
   else {
      snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, " %s => %s", srcAddrStr, dstAddrStr);
      debug_msg(debugMsgBuffer);
      printf("%c|%s.%06ld|%u|%s|0|%s|0|%f, %f|%f, %f\n", CAPTURED_PACKET, tmbuf, header->ts.tv_usec, iphdr->protocol, srcAddrStr, dstAddrStr, src_location_trie->lat, src_location_trie->lng, dst_location_trie->lat, dst_location_trie->lng);
   }

   snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, ", Source: %f, %f, Dest: %f, %f\n", src_location_trie->lat, src_location_trie->lng, dst_location_trie->lat, dst_location_trie->lng);
   debug_msg(debugMsgBuffer);

}

int main(int argc, char **argv) {
   char *dev = NULL, *config_filespec, *geoloc_filespec, *pcap_infilespec, *pcap_outfilespec, errbuf[PCAP_ERRBUF_SIZE];
   const char *pcap_version_string;
   struct stat pcap_stat;
   bool dns_snooping = false;
   bool finished;
   bool expressRead = false;
   char *filterexp = "not ( ether host FF:FF:FF:FF:FF:FF or ip multicast or host 70.75.248.74 )";
   struct bpf_program fp;
   struct stat statbuf;
   struct trie_latlngEntry *geolocdata;
   pcap_t *handle;
   pcap_dumper_t *dumpfilehandle = NULL;

   debuglvl = 1;
   warninglvl = 1;
   setbuf(stdout, NULL);

   /*----------------------------------------------------------------------*/
   /* Get the PCAP version for possible future query by UI.                */
   /*----------------------------------------------------------------------*/
   pcap_version_string = pcap_lib_version();

   /*----------------------------------------------------------------------*/
   /* Set defaults for network device and geoloc data file                 */
   /*----------------------------------------------------------------------*/
// dev = "eth0"; /* Using pcap_lookupdev() for this.                       */
   config_filespec  = "ipgeoloc.cfg";
   geoloc_filespec  = "geoloc_data.txt";
   pcap_outfilespec = "";
   pcap_infilespec  = "";

   /*----------------------------------------------------------------------*/
   /* Process command line parameters.                                     */
   /* ipgeoloc [-i <network device>]                                       */
   /*          [-c <filespec of configuration file>]                       */
   /*          [-f <filespec of geoloc data>]                              */
   /*          [-r <read packets from filename>]                           */
   /*          [-e] Skip delay between packets when reading from file.     */
   /*          [-w write packets to filename]                              */
   /*----------------------------------------------------------------------*/
   opterr = 1;
   int c;
   while ((c = getopt(argc, argv, "i:f:r:w:e")) != -1) {
      switch (c) {
      case 'i':
         dev = optarg;
         break;
      case 'c':
         config_filespec = optarg;
         break;
      case 'f':
         geoloc_filespec = optarg;
         break;
      case 'r':
         if (! stat(optarg, &pcap_stat)) {
            if ( ! S_ISREG(pcap_stat.st_mode) ) {
               error_msg("Specified file is not a regular file (is is a link or socket?).");
               return 1;
            }
            else {
               pcap_infilespec = optarg;
            }
         }
         else {
            perror("stat(): ");
            return 1;
         }
         break;
      case 'w':
         pcap_outfilespec = optarg;
         break;
      case 'e':
         expressRead = true;
         break;
      case '?':
         if (optopt == 'i' || optopt == 'f' || optopt == 'r' || optopt == 'w') {
            snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "Option -%c requires an argument.\n", optopt);
            warning_msg(debugMsgBuffer);
            }
         else if (isprint(optopt)) {
            snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "Unknown option `-%c'.\n", optopt);
            warning_msg(debugMsgBuffer);
            }
         else {
            snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "Unknown option character `\\x%x'.\n", optopt);
            warning_msg(debugMsgBuffer); }
         return 1;
      default:
         abort();
      }
   }

   /*----------------------------------------------------------------------*/
   /* Load the configuration data.                                         */
   /* This is admittedly fragile because it requires a specific format.    */
   /* Currently the only config data is the Google Maps API Key.           */
   /*----------------------------------------------------------------------*/
   if (stat(config_filespec, &statbuf) == 0) {
      FILE* file = fopen(config_filespec, "r");
      if (file != NULL) {
         char line[MAX_GOOGLE_MAPS_API_KEYLENGTH];
         fgets(line, sizeof(line), file);
         sscanf(line, "%s", googleMapsAPIKey);
         fclose(file);
         snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "Setting Google API Key: %s\n", line);
         debug_msg(debugMsgBuffer);
         printf("%c|%s\n", SET_GOOGLE_API_KEY, line);
      } else {
         snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "Error %n opening %s\n", &errno, config_filespec);
         warning_msg(debugMsgBuffer);
         return 2;
      }
   } else {
      snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "Couldn't stat %s (%n) ... Bad path or filename?\n", config_filespec, &errno);
      warning_msg(debugMsgBuffer);
      return (2);
   }

   /*----------------------------------------------------------------------*/
   /* Load the geo-location data.                                          */
   /* This is admittedly fragile because it requires a specific format.    */
   /*----------------------------------------------------------------------*/
   if (stat(geoloc_filespec, &statbuf) == 0) {
      geolocdata = (struct trie_latlngEntry *) malloc(statbuf.st_size);
      if (geolocdata == NULL ) {
         snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "Couldn't allocate %li bytes to hold geoloc data.\n", statbuf.st_size);
         warning_msg(debugMsgBuffer);
         return 2;
      }
      FILE* file = fopen(geoloc_filespec, "r"); /* should check the result */
      if (file != NULL ) {
         char line[1024];
         int lineno = 0;
         /*----------------------------------------------------------------*/
         /* Burn the first line ... it contains header info not data.      */
         /*----------------------------------------------------------------*/
         fgets(line, sizeof(line), file);
         /*----------------------------------------------------------------*/
         /* Now, work through the remaining lines in the file.             */
         /*----------------------------------------------------------------*/
         while (fgets(line, sizeof(line), file)) {
//            debug_msg(line);
            lineno++;
            char *nmptr = strchr(line, '/') + 1;
            int netmask = atoi(nmptr);
            u_int32_t ipAddress;
            float tmplat = 1.0, tmplng = 2.0;

            sscanf(strrchr(line, ',') + 1, "%f", &tmplng);
            *(strrchr(line, ',')) = 0x00;
            sscanf(strrchr(line, ',') + 1, "%f", &tmplat);
            *(strchr(line, '/')) = 0x00;
            inet_pton(AF_INET, line, &ipAddress);

            /*-------------------------------------------------------------*/
            /* Add the entry to the trie structure.                        */
            /*-------------------------------------------------------------*/
            tries_add(geolocdata, ntohl(ipAddress), netmask, tmplat, tmplng);

         }
         snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "There are %u entries in the trie tree.\n", tries_count());
         debug_msg(debugMsgBuffer);
         fclose(file);
      }
      else {
         snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "Error %n opening %s", &errno, geoloc_filespec);
         warning_msg(debugMsgBuffer);
         return 2;
      }
   }
   else {
      snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "Couldn't stat %s ... Bad path or filename?\n", geoloc_filespec);
      warning_msg(debugMsgBuffer);
      return (2);
   }

   dev = pcap_lookupdev(errbuf);
   if (dev == NULL ) {
      snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "Couldn't find default device: %s\n", errbuf);
      warning_msg(debugMsgBuffer);
      return (2);
   }

   /*----------------------------------------------------------------------*/
   /* Open input file to read packets from if user set "-r filename".      */
   /* or open input device to read packets from                            */
   /*----------------------------------------------------------------------*/
   if (strlen(pcap_infilespec) > 0) {
      handle = pcap_open_offline(pcap_infilespec, errbuf);
      if (handle == NULL ) {
         warning_msg(pcap_geterr(handle));
         error_msg("Fatal error - exiting");
      }
      timerclear(&currentPktTS);
      timerclear(&previousPktTS);
      snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "\nReplaying packets from %s.\n", pcap_infilespec);
      debug_msg(debugMsgBuffer);
   }
   else {
      handle = pcap_open_live(dev, TCPIPHEADERLENGTH, true, 1000, errbuf);
      if (handle == NULL ) {
         snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "Couldn't open device %s: %s\n", dev, errbuf);
         warning_msg(debugMsgBuffer);
         warning_msg(pcap_geterr(handle));
         error_msg("Fatal error - exiting");
      }
      snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "\nCapturing from %s.\n", dev);
      debug_msg(debugMsgBuffer);
   }

   /*----------------------------------------------------------------------*/
   /* Open output file to save packets in if user set "-w filename".       */
   /*----------------------------------------------------------------------*/
   if (strlen(pcap_outfilespec) > 0) {
      if ((dumpfilehandle = pcap_dump_open(handle, pcap_outfilespec)) == NULL) {
         warning_msg(pcap_geterr(handle));
      }
   }

   if (pcap_datalink(handle) != DLT_EN10MB) {
      snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "Device %s doesn't provide Ethernet headers - not supported\n", dev);
      warning_msg(debugMsgBuffer);
      return (2);
   }

   /*----------------------------------------------------------------------*/
   /* Setup file descriptors for call to poll() in main loop.              */
   /*----------------------------------------------------------------------*/
   struct pollfd fds[2];
   fds[0].fd      = pcap_get_selectable_fd(handle);
   fds[0].events  = POLLIN || POLLPRI;
   fds[1].fd      = STDIN_FILENO;
   fds[1].events  = POLLIN || POLLPRI;

   /*----------------------------------------------------------------------*/
   /* Main packet processing here.                                         */
   /* - Compile the packet filter.                                         */
   /* - Set the packet filter.                                             */
   /* - Free the packet filter code.                                       */
   /* - Capture packet filter until exit criteria happens*                 */
   /*                                                                      */
   /* * Currently user abort or browser sending an update to the filter exp*/
   /*----------------------------------------------------------------------*/
   finished = false;
   while( ! finished ) {
      if (pcap_compile(handle, &fp, filterexp, 0, PCAP_NETMASK_UNKNOWN) == -1) {
         snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "Couldn't parse filter %s: %s\n", filterexp, pcap_geterr(handle));
         warning_msg(debugMsgBuffer);
         return (2);
      }

      if (pcap_setfilter(handle, &fp) == -1) {
         snprintf(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, "Couldn't install filter %s: %s\n", filterexp, pcap_geterr(handle));
         warning_msg(debugMsgBuffer);
         return (2);
      }

      pcap_freecode(&fp);

      bool capturing_packets = true;
      while( capturing_packets ) {
         int pcap_dispatch_rtnval;


         poll(fds, sizeof(fds) / sizeof(fds[0]), -1);

         /*----------------------------------------------------------------*/
         /* Did we receive a packet from PCAP?                             */
         /*----------------------------------------------------------------*/
         if (fds[0].revents) {
            pcap_dispatch_rtnval = -1; // default to an error condition.
            switch (fds[0].revents) {
            case POLLIN:
            case POLLPRI:  pcap_dispatch_rtnval = pcap_dispatch(handle, 1, process_packet, (u_char *) dumpfilehandle);
                           break;
            case POLLOUT:  debug_msg("Poll fds[0].revents POLLOUT\n");
                           break;
//          case POLLRDHUP:
            case POLLERR:  debug_msg("Poll fds[0].revents POLLERR\n");
                           break;
            case POLLHUP:  debug_msg("Poll fds[0].revents POLLHUP\n");
                           break;
            case POLLNVAL: debug_msg("Poll fds[0].revents POLLNVAL\n");
                           break;
            default:       debug_msg("Poll fds[0].revents Unknown value.\n");
                           break;
            }
            /*----------------------------------------------------------------*/
            /* If we're reading packets from a file, figure out how long to   */
            /* delay before reading the next packet.                          */
            /*----------------------------------------------------------------*/
            if(strlen(pcap_infilespec) > 0) {
               if (! expressRead) {  // Don't wait if user invoked with Express mode
                  if(timerisset(&previousPktTS)) {
                     struct timeval diffPktTime;
                     struct timespec nanoDelay;
                     timersub(&currentPktTS, &previousPktTS, &diffPktTime);
                     nanoDelay.tv_sec  = diffPktTime.tv_sec;
                     nanoDelay.tv_nsec = diffPktTime.tv_usec * 1000;
                     nanosleep(&nanoDelay, NULL);
                  }
               }
            }
            switch ( pcap_dispatch_rtnval ) {
            case -1:
            case -2: pcap_perror(handle, "");
                     capturing_packets = false;
                     finished          = true;
                     break;
            case  0: if(strlen(pcap_infilespec) > 0) {
                        capturing_packets = false;
                        finished          = true; }
                     break;
            case  1: break;
            default: capturing_packets = false;
                     finished          = true;
                     error_msg("Fatal: Unknown response from pcap_dispatch");
            }
         }

         /*----------------------------------------------------------------*/
         /* Did we receive a message from UI?                              */
         /*----------------------------------------------------------------*/
         if (fds[1].revents) {
            debug_msg("Received: ");
            fgets(debugMsgBuffer, JEJ_MAX_DBG_STRLEN, stdin);
            debug_msg(debugMsgBuffer);
            switch (debugMsgBuffer[0]) {
            case     GET_SERVER_VERSION:
                     printf("%c|%s\n", GET_SERVER_VERSION, NO_SERVER_VERSION);
                     break;
            case     GET_PCAP_VERSION:
                     printf("%c|%s\n", GET_PCAP_VERSION, pcap_version_string);
                     break;
            case     GET_BPF_STRING:
                     printf("%c|%s\n", GET_BPF_STRING, filterexp);
                     break;
            case     SET_BPF_STRING:
                     // Check string length
                     // pcap_compile(debugMsgBuffer)
                     // If valid, copy to filterexp; pcap_setfilter(handle, &fp);
                     break;
            case     GET_DNS_SNOOPING:
                     if( dns_snooping ) {
                        printf("%c|DNS SNOOPING ENABLED", GET_DNS_SNOOPING); }
                     else {
                        printf("%c|DNS SNOOPING DISABLED", GET_DNS_SNOOPING); }
                     break;
            case     ENABLE_DNS_SNOOPING:
                     dns_snooping = true;
                     printf("%c|DNS_SNOOPING_ENABLED\n", GET_DNS_SNOOPING);
                     break;
            case     DISABLE_DNS_SNOOPING:
                     dns_snooping = false;
                     printf("%c|DNS_SNOOPING_DISABLED\n", GET_DNS_SNOOPING);
                     break;
            default: printf("%c|Don't know what to do with message type %s\n", (char) UNKNOWN_ERROR, debugMsgBuffer);
                     break;
            }
         }

      }
   }

   /*----------------------------------------------------------------------*/
   /* Close pcap savefile if it was opened.                                */
   /*----------------------------------------------------------------------*/
   if (dumpfilehandle != NULL) {
      pcap_dump_close(dumpfilehandle);
   }
   pcap_close(handle);
   free(geolocdata);
   return 0;
}
