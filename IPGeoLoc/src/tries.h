/*
 * tries.h
 *
 *  Created on: 2015-04-24
 *      Author: jardine
 */

#ifndef TRIES_H_
#define TRIES_H_
#include "ipgeoloc.h"
int tries_add(void *, u_int32_t, int, float, float);
struct trie_latlngEntry *tries_find(u_int32_t);
u_int32_t tries_count();

#endif /* TRIES_H_ */
