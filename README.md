# README #

### What is this repository for? ###
The **IPGeoLoc** is an application using Google Maps* to display the connections your network is making in realtime.

### How do I get set up? ###
**IPGeoLoc** needs three things to be useful:

	1. A managed network switch from which you can sniff your own Internet traffic
	2. A packet capture server that is physically connected to the above switch
	3. A web browser for Google Maps, that can connect to the host above.
		1. Browser compatibility ... is a moving target.  I used Firefox

### Running IPGeoLoc ###
The **IPGeoLoc** packet capture server runs on Linux.  I've developed **IPGeoLoc** on Ubuntu 18.04 but it should build on just about any installation that has GCC installed.

You can run the packet capture and the web browser on the same machine if you have to but it's probably better to split them up.  If you run the web browser on the same machine as the capture server you will have to filter out the browsers' messages to the Google Maps API - not hard, but annoying.

#### Using the pre-compiled image ####
<span style="color:red">This is not recommended.</span>  The reason this **IPGeoLoc** exists is to give you an ability to audit what your network is doing with unknown software.  If you use my binary you don't know what I put into it ... I'm a nice guy but you shouldn't trust anyone - not even nice guys.  If

#### Using Eclipse Oxygen.3a Release (4.7.3a) ####
Download and install the pcap package
Ensure Eclipse is configured with Eclipse Git & the Eclipse C/C++ Development Tools
Clone the [**IPGeoLoc** repository](https://bitbucket.org/JohnEJardine/networkobservatory/src/master/)
Click Project => Build All and in a few seconds the application will build.

#### Minimal build via CLI ####
Don't feel like downloading Eclipse?  You can use [this script]() to build on Ubuntu 18.04 (and probably most debian based systems).

You will need to download the GeoLite IP database from: https://dev.maxmind.com/geoip/geoip2/geolite2/

### Is there other documentation? ###
Glad you asked - Complete presentation documentation is available as [IPGeoLoc.pdf]()

### Contribution guidelines ###
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
If you'd like more information about this get in touch with me (email is the best).
